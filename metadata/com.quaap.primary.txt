Categories:Science & Education
License:GPL-3.0+
Web Site:http://quaap.com/D/Primary
Source Code:https://github.com/quaap/Primary
Issue Tracker:https://github.com/quaap/Primary/issues

Auto Name:Primary
Summary:A simple educational practice game for kids (and adults).
Description:
Primary is a slightly gamified educational application aimed at
Primary(elementary) school-aged children, but of course adults who wish to
sharpen their skills will find it useful as well.  Currently there are levels
dealing with basic math, but I'd like to add other subjects in the future
(spelling, simple geometry, maps, etc?).
.

Repo Type:git
Repo:https://github.com/quaap/Primary

Build:0.1,1
    commit=v0.1
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.1
Current Version Code:1
